package integrationTest

import homework4.Post
import spock.lang.Specification

/**
 * Created by nikl_pod on 11.12.14.
 */
class PostIntegrationSpec extends Specification{
    def "save test"(){
        setup:
            Post post = new Post(name: "Hello!")

        when:
            post.save()

        extend:
            post.id != null
    }
}
