package integrationTest

import homework4.HomeUser
import homework4.Post
import homework4.PostSpec
import spock.lang.Specification

/**
 * Created by nikl_pod on 11.12.14.
 */
class HomeUserIntegrationSpec extends Specification{
    def "save test"(){
        setup:
            HomeUser homeUser = new HomeUser(name: 'Mark')
            Post post = new Post(name: 'Hello!', homeUser: homeUser)

        when:
            homeUser.save()
            post.save()

        expect:
            homeUser.id == post.id

    }
}
