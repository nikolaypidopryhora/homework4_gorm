package homework4

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(HomeUser)
class HomeUserSpec extends Specification {

    void "HomeUser !null"() {
        setup:
            HomeUser homeUser = new HomeUser()

        expect:
            homeUser != null
    }
}
