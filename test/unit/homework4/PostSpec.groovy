package homework4

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Post)
class PostSpec extends Specification {

    void "Post.name !null"() {
        setup:
            Post post = new Post(name: "Hello!")

        expect:
            post.name == 'Hello!'
    }
}
