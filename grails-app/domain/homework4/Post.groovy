package homework4

class Post {
    String name
    
    static belongsTo = [homeUser:HomeUser]

    static constraints = {
        name blank: false
    }
}
