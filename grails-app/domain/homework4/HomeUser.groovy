package homework4

class HomeUser {
    String name
    
    static hasMany = [post:Post]

    static constraints = {
        name blank: false
    }
}
